//imported external dependencies
import mongoose from "mongoose";

//connecting mongoose
mongoose.connect(
  process.env.mongoose_link
);
//importing schema from schema.js
import product_catalog_schema from "./schema.js"

//creating model for products_catalog
const product_catalog = mongoose.model("PRODUCT_CATALOG", product_catalog_schema);

//exporting product_catalog module
export default product_catalog;