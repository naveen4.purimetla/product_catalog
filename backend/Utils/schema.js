//importing mongoose 
import mongoose from "mongoose";

//schema for the products
const Schema = mongoose.Schema; 
const product_catalog_schema = new Schema({
  product_name:        {type:String, required:true},
  product_description: {type:String, required:true},
  product_price:       {type:String, required:true},
  merchant_name:       {type:String, required:true},
  Quantity:            {type:String, required:true},
  discount :           {type:String, required:true}
})


//exporting product_catalog_schema module
export default product_catalog_schema;