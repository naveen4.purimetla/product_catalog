import {
  _productName_helper,
  _productPrice_helper,
  _merchantName_helper,
  _productDescription_helper,
  _Quantity_helper,
} from "../middlewares/Helper.js";

/**
 * This is a middleware function to help with validation of Info.
 * parameters.
 * @param {}  request   HTTP req object as recieved by Express backend
 * @param {*} response   HTTP response object to be populated by Express backend
 * @param {*} next  next() is a function to be called if this middlware validation is success
 * @returns
 */
const _info_validator = (request, response, next) => {
  const { productName, merchantName, Quantity, productPrice, productDescription } =
    request.body;
  const error_object = {};

  error_object.productName           = _productName_helper(productName).productName;
  error_object.merchantName          = _merchantName_helper(merchantName).merchantName;
  error_object.Quantity              = _Quantity_helper(Quantity).Quantity;
  error_object.productPrice          = _productPrice_helper(productPrice).productPrice;
  error_object.productDescription    = _productDescription_helper(productDescription).productDescription;

  for (const [key, value] of Object.entries(error_object)) {
    if (value) {
      return response.status(200).send(error_object);
    }
  }
  next();
};

export default _info_validator ;
