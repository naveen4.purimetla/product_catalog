// Internal Dependencies

/**
 * Helper function to validate Product Name.
 * checks if productName is string, else returns "productName should be of string type"
 * checks if productName is too short, else returns "productName should be at least 2 characters minimum and max 15"
 * @param {} productName
 * @returns
 */
 const _productName_helper = (productName) => {
  const returnObject = {};
  if (typeof productName == "undefined") {
    return;
  }
  if (typeof productName != "string") {
    returnObject.productName = "productName should be of string type";
    return returnObject;
  }
  productName = productName.toString();
  if (productName.length < 2) {
    returnObject.productName =
      "productName length should be at least 2 characters long";
    return returnObject;
  }

  if (productName.length > 15) {
    returnObject.productName =
      "productName length should not be greater than 15 characters long";
    return returnObject;
  }

  return returnObject;
};

/**
 * Helper function to validate price .
 * checks if productPrice is string, else returns "productPrice should be of string type"
 * checks if productPrice is too short, else returns "productPrice should be at least 2 characters minimum and max 15"
 * @param {} productPrice
 * @returns
 */
const _productPrice_helper = (productPrice) => {
  const returnObject = {};
  if (typeof productPrice == "undefined") {
    return;
  }
  if (typeof productPrice !== "string") {
    returnObject.productPrice = "productPrice should be of string type";
    return returnObject;
  }
  productPrice = productPrice.toString();
  if (productPrice.length < 2) {
    returnObject.productPrice =
      "productPrice length should be at least 2 characters long";
    return returnObject;
  }

  if (productPrice.length > 15) {
    returnObject.productPrice =
      "productPrice length should not be greater than 15 characters long";
    return returnObject;
  }

  return returnObject;
};

/**
 * Helper function to validate name
 * checks if contact Number is number, else returns "contact number should be of number type"
 * checks if contact Number is too short, else returns "contact number should be at least 1 characters long and max of 10"
 * checks if contact Number is of the correct format, else returns "contact number should be of the correct format" 406
 * @param {} contactNumber
 * @returns
 */
const _merchantName_helper = (merchantName) => {
  const returnObject = {};
  if (typeof merchantName == "undefined") {
    return;
  }
  if (typeof merchantName != "string") {
    returnObject.merchantName = "merchantName should be of string type";
    return returnObject;
  }
  merchantName = merchantName.toString();
  if (merchantName.length < 2) {
    returnObject.merchantName =
      "merchantName length should be at least 2 characters long";
    return returnObject;
  }

  if (merchantName.length > 15) {
    returnObject.merchantName =
      "merchantName length should not be greater than 15 characters long";
    return returnObject;
  }

  
  return returnObject;
};

/**
 * Helper function to validate description
 * checks if productDescription is string, else returns "productDescription should be of number type"
 * checks if productDescription is too short, else returns "productDescription should be at least 3 characters long and max of 20"
 * @param {} productDescription
 * @returns
 */
const _productDescription_helper = (productDescription) => {
  const returnObject = {};

  if (typeof productDescription != "string") {
    returnObject.productDescription = "productDescription should be of string type";
    return returnObject;
  }
  productDescription = productDescription.toString();
  if (productDescription.length < 3) {
    returnObject.productDescription = "productDescription length should be at least 3 characters long";
    return returnObject;
  }
  if (productDescription.length > 20) {
    returnObject.productDescription =
      "productDescription length should not be greater than 20 characters long";
    return returnObject;
  }
  return returnObject;
};

/**
 * Helper function to validate Quantity.
 * checks if timings is string, else returns "timings should be of string type"
 * checks if timings is too short, else returns "timings should be at least 4 characters minimum and max 15"
 * @param {} timings
 * @returns
 */
const _Quantity_helper = (timings) => {
  const returnObject = {};
  if (typeof timings == "undefined") {
    return;
  }
  if (typeof timings != "string") {
    returnObject.timings = "timings should be of string type";
    return returnObject;
  }
  timings = timings.toString();
  if (timings.length < 4) {
    returnObject.timings =
      "timings length should be at least 4 characters long";
    return returnObject;
  }

  if (timings.length > 15) {
    returnObject.timings =
      "timings length should not be greater than 15 characters long";
    return returnObject;
  }

  return returnObject;
};

// Exporting helper functions
export {
  _productName_helper,
  _productPrice_helper,
  _merchantName_helper,
  _productDescription_helper,
  _Quantity_helper,
};
