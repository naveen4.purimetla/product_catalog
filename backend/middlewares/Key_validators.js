/**
 * Helper function to validate name string
 * @param {} login_data
 * @returns if conditions of name satisfies returns param
 * otherwise returns error
 */
 function product_data_keys_validation(login_data) {
  let param_list = [
    "product_name",
    "product_description",
    "product_price",
    "Quantity",
    "merchant_name",
    "discount",
  ];
  let param = "";
  for (let item in login_data) {
    if (!param_list.includes(item)) {
      param = item;
    }
  }
  return param;
}

/**
 * Helper function to validate name string
 * @param  login_data
 * @returns checks for empty fields if not satisfies returns false
 * otherwise it returns true
 */
function empty_login_fields(login_data) {
  const { product_name, product_description, product_price, merchant_name, Quantity, discount } = login_data;
  if (
    product_name === "" ||
    product_description === "" ||
    merchant_name === "" ||
    product_price === "" ||
    Quantity === "" ||
    discount ===""
  ) {
    return true;
  }
  return false;
}

const validation_for_product_data = (request, response, next) => {
  const data = request.body;
  //initializing empty_fields as empty_login_fields
  const empty_fields = empty_login_fields(data);
  //initializing keys_validation as login_data_keys_validation
  const keys_validations = product_data_keys_validation(data);
  if (keys_validations) {
    response.send(
      JSON.stringify({
        status_code: 422,
        status_message: `${keys_validations} is not a valid key`,
      })
    );
  } else if (empty_fields) {
    response.send(
      JSON.stringify({
        status_code: 401,
        status_message: "all fields must not be empty",
      })
    );
  } else {
    next();
  }
};

export default validation_for_product_data ;
