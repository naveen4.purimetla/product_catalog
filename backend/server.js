//importing express 
import express from "express";
//importing body-parser
import bodyParser from "body-parser";
//port number for the server 
// const PORT = 8000;
//creating an instance of express
const app = express();
//returns middleware that only parses json
app.use(bodyParser.json());
//returns middleware that only parses urlencoded bodies
app.use(bodyParser.urlencoded({ extended: true }));

//importing .env configuration 
import "dotenv/config";
//importing cors
import cors from "cors";
//enable the express server to respond to preflight requests
app.use(
  cors({
    origin: "*",
  })
);

//importing path_handlers for the api's
import post_product_details from "./path_handlers/post_apis.js"
import get_all_products_details from "./path_handlers/get_apis.js"
import delete_product_details from "./path_handlers/delete_apis.js"
import update_product_details from "./path_handlers/put_apis.js"

//importing validators
import validation_for_product_data from "../backend/middlewares/Key_validators.js"
import _info_validator from "../backend/middlewares/Database_validators.js"

//API to get the products in the database
app.get("/data",get_all_products_details);

//API to post the products in the database
app.post("/post_product_details", validation_for_product_data,  post_product_details);

//API to update the products in the database
app.put("/update_product_details",update_product_details);

//API to delete the products in the database
app.delete("/delete_product",delete_product_details);

// app.get("/data/:id",get_product_by_id)


//port number to check the server is running or not 
app.listen(process.env.backend_port, () => {console.log(`The port is running @ ${process.env.backend_port}`);});
