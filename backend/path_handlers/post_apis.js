//importing model
import product_catalog from "../Utils/model.js";

/**
 * @param post_product_details posts the details of the products
 * @param {*} request HTTP req object as recieved by Express backend
 * @param {*} response HTTP response object to be populated by Express backend
 */

const post_details = [];
const post_product_details = (request, response) => {
  const product_name = request.body.product_name;
  const product_description = request.body.product_description;
  const product_price = request.body.product_price;
  const merchant_name = request.body.merchant_name;
  const Quantity = request.body.Quantity;
  const discount = request.body.discount;

  const post_data = {
    product_name: product_name,
    product_description: product_description,
    product_price: product_price,
    merchant_name: merchant_name,
    Quantity: Quantity,
    discount:discount,
  };
//saving the data using save()function to the database   
const posts_data = new product_catalog(post_data);
posts_data.save().then(() => response.send(JSON.stringify({
  status_code:200,
  status_message:"data saved successfully"
})));
post_details.push(posts_data);
};


//exporting post_product_details module
export default post_product_details;