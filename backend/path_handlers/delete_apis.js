//importing model
import product_catalog from "../Utils/model.js";

/**
 * @param delete_product_details deletes the details of the products
 * @param {*} request HTTP req object as recieved by Express backend
 * @param {*} response HTTP response object to be populated by Express backend
 */

 const delete_product_details = (request, response) => {
  const {product_name} = request.body;
  product_catalog.findOneAndRemove({ product_name: product_name }, (err) => {
    if (err) {
      console.log(err);
    } else {
      response.send(
        JSON.stringify({
          status_code: 200,
          status_message: "data deleted successfuly",
        })
      );
    }
  });

};

//exporting delete_product_details module
export default delete_product_details;