//importing model
import product_catalog from "../Utils/model.js";

/**
 * @param update_product_details updates the details of the products
 * @param {*} request HTTP req object as recieved by Express backend
 * @param {*} response HTTP response object to be populated by Express backend
 */

const update_product_details = async (request,response)=>{
  const data = request.body
  console.log(data)
  try {
    const result = await product_catalog.updateOne(
      { product_name: data.product_name },
      {
        $set: {
          product_price: data.price,
          discount: data.discount,
          product_description:data.product_description,
          Quantity:data.Quantity
          
        },
      }
      );
      response.send(JSON.stringify({
          status_code: 200,
          status_message: "data updated successfully",
          name : data.product_name
    
      }))

      console.log(result)
    } catch (err) {
      //catch block catches the exception
      response.send(err);

    }
  // response.send("updates done for the product")

}


//exporting update_product module
export default update_product_details;