//importing model
import product_catalog from "../Utils/model.js";

/**
 * @param get_all_products_details gets the details of all products.
 * @param {*} request HTTP req object as recieved by Express backend
 * @param {*} response HTTP response object to be populated by Express backend
 */

 const get_all_products_details =(request, response) => {
  product_catalog.find({}, (error, data) => {
    if (error) {
      console.log(error);
    } else {
      // send the data to the response
      response.send(data);
    }
  });
};

//  export const get_product_by_id = async(req,res)=>{
//    const {id} = req.params

//    try{
//     const product = await product_catalog.findById(id)
//     res.status(200).json(product)
//    }catch(error){
//     res.status(400).json({error})
//    } 
// }

// exporting get_all_products_details module
export default get_all_products_details;
