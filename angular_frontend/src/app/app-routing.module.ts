import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateProductComponent } from './components/create-product/create-product.component';
import { DeleteProductComponent } from './components/delete-product/delete-product.component';
import { ProductViewComponent } from './components/product-view/product-view.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { UpdateProductComponent } from './components/update-product/update-product.component';

const routes: Routes = [
  { path: "", component: ProductViewComponent },
  { path: "side_bar", component: SidebarComponent },
  { path: "create_product", component: CreateProductComponent },
  { path: "update_product", component: UpdateProductComponent },
  { path: "delete_product", component: DeleteProductComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
