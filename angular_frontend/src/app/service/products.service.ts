import { Injectable } from '@angular/core';
import { HttpClient ,HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class ProductsService {
  products: any;
  url: string = `http://localhost:8000`;
  constructor(private http: HttpClient) {}
  GetProducts() {
    return this.http.get(`${this.url}/data`);
  }

  NewProducts(productData: any) {
    return this.http.post(`${this.url}/post_product_details`, productData);
  }
 
  UpdateProduct(Data:any){
    console.log("data",Data)
    return this.http.put(`${this.url}/update_product_details`, Data)
  }

  DeleteProduct(productName:any){
     const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        product_name:productName
      },
    };
  
    return this.http.delete(`${this.url}/delete_product`,options)
    .subscribe((s) => {
      console.log(s);
    })
  }

  GetProductsById(id:any) {
    return this.http.get(`${this.url}/data/`+ id);

  }

}
