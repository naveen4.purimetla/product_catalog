import { Component, OnInit } from '@angular/core';
import { ProductsService } from 'src/app/service/products.service';

@Component({
  selector: 'app-product-view',
  templateUrl: './product-view.component.html',
  styleUrls: ['./product-view.component.css']
})
export class ProductViewComponent implements OnInit {
  products:any;
  constructor( private service:ProductsService) { 
    this.service.GetProducts().subscribe((result)=>{
      this.products = result;
      console.log(this.products)
      
    }
    )
  }

  ngOnInit(): void {
  }

}
