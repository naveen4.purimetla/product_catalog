import { Component, OnInit } from '@angular/core';
import { ProductsService } from 'src/app/service/products.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-update-product',
  templateUrl: './update-product.component.html',
  styleUrls: ['./update-product.component.css']
})
export class UpdateProductComponent implements OnInit {
  
  product_name:any
  Productdata : any
  // Data = {
  //   product_name: '',
  //   product_description: '',
  //   product_price: '',
  //   Quantity: '',
  //   discount: ''
  // }
  constructor( private service : ProductsService, private router: Router) {
    this.Productdata= this.router.getCurrentNavigation()?.extras.state
   console.log("Product",this.Productdata.product)
   }
  

  ngOnInit(): void {
      console.log("history",history.state)
  }

  getProductName(event: any) {
    this.Productdata.product.product_name = event.get.value;
    // console.log('Data', this.Data.product_name);
  }

  getProductDescription(event: any) {
    this.Productdata.product.product_description = event.target.value;
  }

  getProductPrice(event: any) {
    this.Productdata.product.product_price= event.target.value;
  }

  getQuantity(event: any) {
    this.Productdata.product.Quantity = event.target.value;
  }

  getDiscount(event: any) {
    this.Productdata.product.discount = event.target.value;
  }

  updateProductsData(){
    // console.log(this.Data)
    this.Productdata.product.price = this.Productdata.product.product_price
     this.service.UpdateProduct(this.Productdata.product).subscribe((Data) => console.log('products', Data))
  }
}
