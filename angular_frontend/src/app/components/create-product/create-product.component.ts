import { Component, OnInit } from '@angular/core';
import { ProductsService } from 'src/app/service/products.service';

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.css'],
})
export class CreateProductComponent implements OnInit {
  productData = {
    product_name: '',
    product_description: '',
    product_price: '',
    Quantity: '',
    discount: '',
    merchant_name: '',
  };
  constructor(private service: ProductsService) {}

  ngOnInit(): void {}

  getProductName(event: any) {
    this.productData.product_name = event.target.value;
    // console.log('Data', this.productData);
  }

  getProductDescription(event: any) {
    this.productData.product_description = event.target.value;
  }

  getProductPrice(event: any) {
    this.productData.product_price = event.target.value;
  }

  getMerchantName(event: any) {
    this.productData.merchant_name = event.target.value;
  }

  getQuantity(event: any) {
    this.productData.Quantity = event.target.value;
  }

  getDiscount(event: any) {
    this.productData.discount = event.target.value;
  }

  addProductsData() {
    // console.log("DATA");
    this.service
      .NewProducts(this.productData)
      .subscribe((data) => console.log('Products', data));
  }
}
