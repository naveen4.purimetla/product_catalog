import { Component, OnInit } from '@angular/core';
import { ProductsService } from 'src/app/service/products.service';

@Component({
  selector: 'app-delete-product',
  templateUrl: './delete-product.component.html',
  styleUrls: ['./delete-product.component.css']
})
export class DeleteProductComponent implements OnInit {
  
  productName :string = "";
  constructor( private service: ProductsService ) { }

  ngOnInit(): void {
  }

  getProductName(event: any) {
    this.productName= event.target.value;
    // console.log('Data', this.productName);
  }

  DeleteProducts(){
    console.log('Data', this.productName);
    this.service.DeleteProduct(this.productName)
  }
}
