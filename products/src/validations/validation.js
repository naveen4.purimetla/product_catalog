//validation for name of the product
function validate_product_name(product_name) {
  if (product_name === "") {
    return "*Product name should not be empty";
  } else if (product_name < 5 || product_name > 20) {
    return "*Product name must have min 5 chars and max 50";
  } else {
    return "";
  }
}

//validation part for the description of the product
function validate_product_description(product_description) {
  if (product_description.length === 0) {
    return "*Description is a mandatory parameter";
  } else if (product_description.length < 10 || product_description.length > 45) {
    return "*Description should be of length minimum 50 chars";
  } else if (product_description.includes("  ")) {
    return "*Description shouldn't consist of consecutive spaces";
  } else {
    return "";
  }
}

//validation part for the price of the product
function validate_product_price(product_price) {
  if (product_price.length === 0) {
    return "*product_price should not be empty";
  } else if (product_price < 0) {
    return "*product_price should not be less than zero.";
  } else {
    return "";
  }
}

//validation part for the quantity of the product
function validate_Quantity(Quantity) {
  if (Quantity.length === 0) {
    return "*Quantity should not be empty";
  } else if (isNaN(Quantity)) {
    const Quantity_num = parseFloat(Quantity);
    if (!Number.isInteger(Quantity_num)) {
      return "*Quantity should be an Integer";
    }
  } else {
    const Quantity_num = parseFloat(Quantity);
    if (!Number.isInteger(Quantity_num) || Quantity_num < 0) {
      return "*Quantity should be an integer and must be greater than or equal to zero";
    } else {
      return "";
    }
  }
}

//validation part for the name related to the merchant
function validate_merchant_name(merchant_name) {
  if (merchant_name.length === 0) {
    return "*Merchant name should not be empty";
  } else if (merchant_name.length < 5 || merchant_name.length > 50) {
    return "*Merchant name must have min 5 chars and max 50";
  } else {
    return "";
  }
}

//validation part for the discount part
function validate_discount(discount) {
  if (discount.length === 0) {
    return "*discount should not be empty";
  } else if (discount < 0) {
    return "*discount should not be less than zero.";
  } else {
    return "";
  }
}

//validation part for price
function validate_price(price) {
  if (price.length === 0) {
    return "*price should not be empty";
  } else if (price < 0) {
    return "*price should not be less than zero.";
  } else {
    return "";
  }
}

//validation part for ID
function validate_id(id) {
  if (id.length === 0) {
    return "*ID should not be empty";
  } else if (id < 0) {
    return "*ID should not be less than zero.";
  } else {
    return "";
  }
}

//exporting all validation modules
export {
  validate_product_name,
  validate_product_description,
  validate_Quantity,
  validate_product_price,
  validate_merchant_name,
  validate_discount,
  validate_price,
  validate_id,
};
