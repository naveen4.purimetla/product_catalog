//imported css for styling 
import './App.css';

//imporetd external modules
import { BrowserRouter, Switch, Route } from "react-router-dom";
//imported internal modules

import Header from './Components/Header/header';
import ProductView from './Components/ProductView/productview';
import Sidebar from './Components/Sidebar/sidebar';
import CreateProduct from './Components/CreateProduct/createproduct';
import UpdateProduct from "./Components/UpdateProduct/updateproduct";
import DeleteProduct from "./Components/DeleteProduct/deleteproduct";

//function app dispaly the all components data and placed one on one 
function App() {
  //returns the data from all the components at one place and dispalys as per the routing done
  return (
    <BrowserRouter>
    <div>
      <Header/>
      <div className='main-div-app'>
      <Sidebar/>
      <Switch>
        <Route exact path="/products" component={ProductView}/>
        <Route exact path="/create_product" component={CreateProduct}/>
        <Route exact path="/update_product" component={UpdateProduct}/>
        <Route exact path="/delete_product" component={DeleteProduct}/>
      </Switch>
      </div>
    </div>
    </BrowserRouter>
  );
}

//exporting app module
export default App;
