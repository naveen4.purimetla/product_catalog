//importing CSS file for styling
import "./header.css"

//class component for header 
const Header = ()=>{
   //returns the header which displays the name and the project name
    return(
        <div className="header">
          <h2>Products Catalog</h2>
          <div className="profile">
             <label>Naveen.P</label>
          </div>
       </div>
    )
}

//exporting header module
export default Header