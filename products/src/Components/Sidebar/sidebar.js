//imported css file for styling the sidebar view
import "./sidebar.css"

//class component for the sidebar 
const Sidebar = ()=>{
    //returns the data to the User Interface, which moves to different forms 
    return(
        <div>
        {/* The sidebar */}
        <div className="sidebar">
          <a className="" href="/products">Products</a>
          <a href="/create_product">Create Product</a>
          <a href="/update_product">Update Product</a>
          <a href="/delete_product">Delete Product</a>
        </div>
        {/* Page content */}
        <div className="content">
          
        </div>
      </div>
    )
}

//exporting sidebar module
export default Sidebar