//impoted react component
import React, { Component } from "react";
//imported css for styling
import "./createproduct.css";

import {
  validate_product_name,
  validate_product_description,
  validate_product_price,
  validate_Quantity,
  validate_merchant_name,
  validate_discount,
} from "../../validations/validation.js";

//class component for creatig the product
class CreateProduct extends Component {
  //using state for the user to update the products
  state = {
    product_name: "",
    product_description: "",
    product_price: "",
    discount: "",
    merchant_name: "",
    Quantity: "",
    product_name_err: "",
    product_price_err: "",
    product_description_err: "",
    Quantity_err: "",
    merchant_name_err: "",
    discount_err: "",
  };

  //using change function with the help of setState to update the product name
  changeProductName = (event) => {
    this.setState({ product_name: event.target.value });
  };

  //using change function with the help of setState to update the product description
  changeProductDescription = (event) => {
    this.setState({ product_description: event.target.value });
  };

  //using change function with the help of setState to update the product price
  changeProductPrice = (event) => {
    this.setState({ product_price: event.target.value });
  };

  //using change function with the help of setState to update the product discount
  changeDiscount = (event) => {
    this.setState({ discount: event.target.value });
  };

  //using change function with the help of setState to update the merchant name
  changeMerchantName = (event) => {
    this.setState({ merchant_name: event.target.value });
  };

  //using change function with the help of setState to update the product Quantity
  changeQuantity = (event) => {
    this.setState({ Quantity: event.target.value });
  };

  //submitting the user data using the state and setState function and updating to the API call
  submitingUserData = async (event) => {
    const product_name_err = validate_product_name(this.state.product_name);
    if (product_name_err !== "") {
      this.setState({ product_name_err: product_name_err });
    }

    const product_description_err = validate_product_description(
      this.state.product_description
    );
    if (product_description_err !== "") {
      this.setState({ product_description_err: product_description_err });
    }

    const product_price_err = validate_product_price(this.state.product_price);
    if (product_price_err !== "") {
      this.setState({ product_price_err: product_price_err });
    }

    const Quantity_err = validate_Quantity(this.state.Quantity);
    if (Quantity_err !== "") {
      this.setState({ Quantity_err: Quantity_err });
    }

    const merchant_name_err = validate_merchant_name(this.state.merchant_name);
    if (merchant_name_err !== "") {
      this.setState({ merchant_name_err: merchant_name_err });
    }

    const discount_err = validate_discount(this.state.discount);
    if (discount_err !== "") {
      this.setState({ discount_err: discount_err });
    }

    event.preventDefault();

    this.setState({ success_msg: "" });
    this.setState({ failure_msg: "" });

    const url = "http://localhost:8000/post_product_details";
    const {
      product_name,
      product_description,
      product_price,
      Quantity,
      merchant_name,
      discount,
    } = this.state;
    const bodyData = {
      product_name,
      product_description,
      product_price,
      Quantity,
      merchant_name,
      discount,
    };
    const option = {
      method: "POST",
      body: JSON.stringify(bodyData),
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };

    if (
      product_name_err === "" ||
      product_description_err === "" ||
      product_price_err === "" ||
      Quantity_err === "" ||
      merchant_name_err === "" ||
      discount_err === ""
    )
      // ) {
      //   const response = await fetch(url, option);
      //   console.log(response);
      //   const data = await response.json();
      //   console.log(data);
      try {
        const response = await fetch(url, option);

        if (response.status === 200) {
          const msg = "Product Added Successfully";
          this.setState({ success_msg: msg });
          alert(msg);

          const { history } = this.props;
          history.replace("/products");
          
        } else {
          const msg = "Product Not Added";
          this.setState({ failure_msg: msg });
          alert(msg);
        }
      } catch {
        const msg = "unable to connect the server";
        this.setState({ failure_msg: msg });
        alert(msg);
      }
  };
  //renders the component to update the product
  render() {
    //returns the data what the render function need to be displayed to the USER
    return (
      <div className="form animated flipInX">
        <h2>Create Product</h2>
        <form onSubmit={this.submitingUserData}>
          <input
            placeholder="Enter Product Name"
            type="text"
            onChange={this.changeProductName}
          />
          <p> {this.state.product_name_err}</p>
          <input
            placeholder="Enter Product Description"
            type="text"
            onChange={this.changeProductDescription}
          />
          <p> {this.state.product_description_err}</p>
          <input
            placeholder="Enter Product price"
            type="text"
            onChange={this.changeProductPrice}
          />
          <p> {this.state.product_price_err}</p>
          <input
            placeholder="Enter Discount"
            type="text"
            onChange={this.changeDiscount}
          />
          <p>{this.state.discount_err}</p>
          <input
            placeholder="Enter Merchant Name"
            type="text"
            onChange={this.changeMerchantName}
          />
          <p> {this.state.merchant_name_err}</p>
          <input
            placeholder="Enter Quantity"
            type="text"
            onChange={this.changeQuantity}
          />
          <p> {this.state.Quantity_err}</p>
          <button type="submit">Create</button>
        </form>
      </div>
    );
  }
}

//exporting createproduct module
export default CreateProduct;
