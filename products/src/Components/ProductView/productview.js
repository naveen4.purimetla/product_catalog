//imported external module
import { Component } from "react";
//imported internal module
import Product from "../Product/product";
//imported css file for styling
import "./productview.css";

//class component for productview to view the products
class ProductView extends Component {
  state = {
    search: "",
    productData: [],
    searchFilter: [],
  };

  onSubmitSearch = (event) => {
    event.preventDefault();
    this.componentDidMount();
  };

  componentDidMount() {
    this.productApiCall();
  }

  changeSearch = (event) => {
    console.log(event.target.value);
    // this.setState({ product_name: event.target.value });
    this.setState({ search: event.target.value });
  };

  //using an API call to gets the data from the products section
  productApiCall = async () => {
    const url = "http://localhost:8000/data";
    const option = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };
    const response = await fetch(url, option);
    console.log(response);
    const data = await response.json();
    console.log(data);
    this.setState({ productData: data });
  };

  //renders the component and gets the data from the product and view in the User Interface
  render() {
    const { productData, search } = this.state;
    const searchItem = productData.filter((item) =>
      item.product_name.includes(search)
    );
    console.log("search item", searchItem);
    //returns the data what the render function need to be displayed to the USER
    return (
      <>
        <div className="og-contianer">
          <div className="setting">
            <h1 className="heading-line">Products</h1>
            <div className="search-div">
              <input
                type="text"
                id="search"
                placeholder="Search Product"
                onChange={this.changeSearch}
              />
              <button className="search-btn ">Search</button>
            </div>
          </div>

          <div className="og-row og-li og-li-head">
            <div className="og-li-col og-li-col-2 text"> Name</div>
            <div className="og-li-col og-li-col-3 text-center text">
              Description
            </div>
            <div className="og-li-col og-li-col-4 text-center text">Price</div>
            <div className="og-li-col og-li-col-5 text-center text">
              Discount
            </div>
            <div className="og-li-col og-li-col-6 text-center text">
              Merchant name
            </div>
            <div className="og-li-col og-li-col-7 text-center text">
              Quantity
            </div>
            <div className="og-li-col og-li-col-8 text-center text">
              Actions
            </div>
          </div>
          {searchItem.map((each) => (
            <Product each={each} key={each.id} />
          ))}
          {/* {searchItem.length === 0 ? 
        <div><h1>Not Found</h1></div>  :<div>
          {searchItem.map((each)=>(
            <Product each={each} key={each.id}/>
          ))} */}
          {/* </div> */}
        </div>
      </>
    );
  }
}

//exporting productview module
export default ProductView;
