import "./SearchProduct.css";

const SearchProducts = (props) => {
  const { each } = props;
  return (
    
    <tr className="mt-4">
      <th className="Details">{each.product_name}</th>
      <th className="Details"> {each.product_description}</th>
      <th className="Details"> {each.Quantity}</th>
      <th className="Details"> {each.merchant_name}</th>
      <th className="Details"> {each.product_price}</th>
    </tr>
    
  );
};

export default SearchProducts;




