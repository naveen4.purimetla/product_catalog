//importing component for react
import { Component } from "react";
import { validate_product_name } from "../../validations/validation";

//class component to delete the product data
class DeleteProduct extends Component {
  state = {
    product_name: "",
    product_name_err: "",
  };

  //using change function with the help of setState to delet the product using ID
  changeProductName = (event) => {
    this.setState({ product_name: event.target.value });
  };

  //submitting the form to the api call to delete the product
  //used AWAIT and ASYNC to fetch the data from the database and delete the product data.
  submitingUserData = async (event) => {
    
    const product_name_err = validate_product_name(this.state.product_name);
    if (product_name_err !== "") {
      this.setState({ product_name_err: product_name_err });
    }

    event.preventDefault();
    this.setState({ success_msg: "" });
    this.setState({ failure_msg: "" });

    const url = "http://localhost:8000/delete_product";
    const { product_name } = this.state;
    const bodyData = {
      product_name,
    };
    const option = {
      method: "DELETE",
      body: JSON.stringify(bodyData),
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json",
      },
    };
    if (product_name_err === "")
      // ) {
      //   const response = await fetch(url, option);
      //   console.log(response);
      //   const data = await response.json();
      //   console.log(data);
      try {
        const response = await fetch(url, option);

        if (response.status === 200) {
          const msg = "Product Deleted Successfully";
          this.setState({ success_msg: msg });
          alert(msg);

          const { history } = this.props;
          history.replace("/products");
          
        } else {
          const msg = "Product Not Deleted";
          this.setState({ failure_msg: msg });
          alert(msg);
        }
      } catch {
        const msg = "unable to connect the server";
        this.setState({ failure_msg: msg });
        alert(msg);
      }
  };
  //renders the component for the User to dalate the product
  render() {
    //returns the data what the render function need to be displayed to the USER
    return (
      <div className="form animated flipInX">
        <h2>Delete product</h2>
        <form onSubmit={this.submitingUserData}>
          <input placeholder="Product Name" type="text" onChange={this.changeProductName} />
          <p>{this.state.product_name_err}</p>
          <button type="submit">Delete</button>
        </form>
      </div>
    );
  }
}

//exporting deleteproduct module
export default DeleteProduct;
