//importing external module
import { Component } from "react";

//importing validations
import {
  validate_discount,
  validate_price,
  validate_product_name,
  validate_product_description,
  validate_Quantity,
} from "../../validations/validation";

//class component to update the product details based on the state provided
class UpdateProduct extends Component {
  state = {
    product_name: this.props.location.state.product_name,
    price: this.props.location.state.product_price,
    discount: this.props.location.state.discount,
    product_description: this.props.location.state.product_description,
    Quantity: this.props.location.state.Quantity,
    Quantity_err: "",
    product_name_err: "",
    price_err: "",
    discount_err: "",
    product_description_err: "",
  };

  //using change function with the help of setState to update the product
  changeProductName = (event) => {
    this.setState({ product_name: event.target.value });
  };

  //using change function with the help of setState to update the product price
  changePrice = (event) => {
    this.setState({ price: event.target.value });
  };

  //using change function with the help of setState to update the product discount
  changeDiscount = (event) => {
    this.setState({ discount: event.target.value });
  };

  //using change function with the help of setState to update the product Description
  changeProductDescription = (event) => {
    this.setState({ product_description: event.target.value });
  };

  //using change function with the help of setState to update the Quantity
  changeQuantity = (event) => {
    this.setState({ Quantity: event.target.value });
  };
  //submitting the data to the api to update the product details
  //using ASYNC and AWAIT to fetch the data in DB and update them as per the requirement

  submitingUpdatedData = async (event) => {

    const price_err = validate_price(this.state.price);
    if (price_err !== "") {
      this.setState({ price_err: price_err });
    }

    const discount_err = validate_discount(this.state.discount);
    if (discount_err !== "") {
      this.setState({ discount_err: discount_err });
    }

    const product_name_err = validate_product_name(this.state.id);
    if (product_name_err !== "") {
      this.setState({ product_name_err: product_name_err });
    }

    const product_description_err = validate_product_description(
      this.state.product_description
    );
    if (product_description_err !== "") {
      this.setState({ product_description_err: product_description_err });
    }

    const Quantity_err = validate_Quantity(this.state.Quantity);
    if (Quantity_err !== "") {
      this.setState({ Quantity_err: Quantity_err });
    }

    event.preventDefault();

    this.setState({ success_msg: "" });
    this.setState({ failure_msg: "" });

    const url = "http://localhost:8000/update_product_details";
    const { product_name, product_description, Quantity,price, discount } = this.state;
    const bodyData = {
      product_name,
      price,
      discount,
      product_description,
      Quantity,
    };

    const option = {
      method: "PUT",
      body: JSON.stringify(bodyData),
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json",
      },
    };
    if (
      price_err === "" ||
      discount_err === "" ||
      product_name_err === "" ||
      product_description_err === "" ||
      Quantity_err === ""
    )
      try {
        const response = await fetch(url, option);

        if (response.status === 200) {
          const msg = "Product Updated Successfully";
          this.setState({ success_msg: msg });
          alert(msg);
          
          const { history } = this.props;
          history.replace("/products"); 

        } else {
          const msg = "Product Not Updated";
          this.setState({ failure_msg: msg });
          alert(msg);
        }
      } catch {
        const msg = "unable to connect the server";
        this.setState({ failure_msg: msg });
        alert(msg);
      }
  };
  //renders the component, which is used to update the product data by the USER
  render() {
    //console.log(this.props.location.state)
    const { product_description, product_name, price, Quantity, discount } =
      this.state;

    //returns the data what the render function need to be displayed to the USER
    return (
      <div className="form animated flipInX">
        <h2>Update product</h2>
        <form onSubmit={this.submitingUpdatedData}>
          <input
            placeholder="Product Name"
            type="text"
            onChange={this.changeProductName}
            value={product_name}
          />
          <p>{this.state.product_name_err}</p>
          <input
            placeholder="Price"
            type="text"
            onChange={this.changePrice}
            value={price}
          />
          <p>{this.state.price_err}</p>
          <input
            placeholder="Discount"
            type="text"
            onChange={this.changeDiscount}
            value={discount}
          />
          <p>{this.state.discount_err}</p>
          <input
            placeholder="Description"
            type="text"
            onChange={this.changeProductDescription}
            value={product_description}
          />
          <p>{this.state.product_description_err}</p>
          <input
            placeholder="Quantity"
            type="text"
            onChange={this.changeQuantity}
            value={Quantity}
          />
          <p>{this.state.Quantity_err}</p>
          <button type="submit">Update</button>
        </form>
      </div>
    );
  }
}

//exporting updateproduct module
export default UpdateProduct;
