//importing css for styling
import "./product.css";
import { Link } from "react-router-dom";

//class component for the products to be displayed
const Product = (props) => {
  const { each } = props;
  //returns the data in the user interface
  return (
    <div className="data-row og-row og-li Experienced Engineering 7.3 Ready to hire Andhra Pradesh Yes">
      <div className="og-li-col og-li-col-2 text">{each.product_name}</div>
      <div className="og-li-col og-li-col-3 text text-center">
        {each.product_description} 
      </div>
      <div className="og-li-col og-li-col-4 text text-center">
        {each.product_price}
      </div>
      <div className="og-li-col og-li-col-5 text text-center">
        {each.discount}
      </div>
      <div className="og-li-col og-li-col-6 text text-center">
        {each.merchant_name}
      </div>
      <div className="og-li-col og-li-col-7 text text-center">
        {each.Quantity}
      </div>
      <div>
        {each.Actions}
      </div>
      <Link
        to={{
          pathname: "/update_product",
          state: each,
        }}
      >
        <button className="B1">Update</button>
      </Link>
      <Link
        to={{
          pathname: "/delete_product",
          state: each,
        }}
      >
        <button className="B2">Delete</button>
      </Link>
    </div>
  );
};

//exporting product module
export default Product;
